import numpy as np

from scripts import vmm
from scripts import constrainedVMM
from scripts import Converter

#training data
alphabet = [1,2,3,4,5,6]
dataset = [np.asarray([1,2,3,1,4,1,5,1,2,3,1])]

#creating VMM and training it
myMarkov = vmm.VMM(alphabet = alphabet)
myMarkov.learn(dataset, 5)

#different kind of sampling
print("Sampling from Learned Distribution: ", myMarkov.sample(4, np.asarray([1,2,3])))
print("Sampling from mean-distribution with Hamming-Distance = 2:  ", myMarkov.sample_with_masks(4, np.asarray([1,2,3]),2))

"""
Solving a CSP
"""
#creating solver 
solver = constrainedVMM.VMMCSP(alphabet = alphabet, vomm= myMarkov)
#creating constraints
solver.create_constraints(np.array([None,lambda x: x == 1,lambda x: x == 2,None,None,lambda x: x == 1,None]),np.array([[(0,2),lambda x,y: x > y], [(0,6), lambda x,y: x == y]]))

#solving CSP
solver.solve()

#generating sequence
print("Sequence that fulfills CSP: ",solver.generate(3))
"""
Converting multiple viewpoints to a single ing
"""

#data used
pitch_alphabet_size = 128
duration_alphabet_size = 12
other_alphabet_size = 10
pitch_viewpoint = [60, 48, 52, 54, 60, 60, 60, 70, 64]
duration_viewpoint = [0, 11, 4, 5, 5, 4, 8, 8, 8]
random_viewpoint = [0, 1, 4, 5, 7, 8, 9, 0, 1]

conv = Converter.Convert(3, [pitch_alphabet_size,duration_alphabet_size,other_alphabet_size])
vps = [pitch_viewpoint,duration_viewpoint,random_viewpoint]

#converting vps to int
ints = conv.convert_vps_to_ints(vps)
print(ints , 'int representation of multiple viewpoints')
print(conv.convert_ints_to_vps(ints), 'vps reconstructed')