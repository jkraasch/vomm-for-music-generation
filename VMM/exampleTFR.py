import tensorflow as tf
import numpy as np
import sys
sys.path.append("..")

from style_machine.lib_dataset import *
from style_machine.lib_piano_roll import *
from style_machine.lib_grid import *

"""
Creating Training-Data
Opening tf.records add their piano rolls to training data
"""
grid = make_grid(
    duplet_resolution=16,
    triplet_resolution=16)

table_iter = table_dataset(
    tfrecords_directory="LMD_matched_w_key_chord",
    as_numpy=True)

# get 10 pieces in a random order
piano_rolls = []
for table,i in zip(table_iter, range(20)):
    piano_roll = PianoRoll(table,grid)
    if i %9 == 0:
        print(piano_roll.to_onset_and_tie().astype(int).shape)
    piano_rolls.append(piano_roll.to_onset_and_tie().astype(int))


"""
Converting pianoroll-data to a 1-Dimensional sequence
getting alphabet
"""
from vmmPython import Converter

alphabet = []
piano_roll = piano_rolls[0]
#getting alphabet sizes of every viewpoint, as a pianoroll has is univariate,
#the alphabet size is 2 for every viewpoint
for _ in range(len(keyboard.T)):
    alphabet.append(2)

#instantiating the converter 
converter = Converter.Convert(len(alphabet), alphabet)

#converting multidimensional pianorolls to list of 1-dimensional sequences 
sequences = []
for piano_roll in piano_rolls:
    sequences.append(converter.convert_vps_to_ints(piano_roll.T))


"""
Creating vmm and training it on training-data
"""
from vmmPython import vmm

#getting the alphabet from all the sequences
allalpha = []
for seq in sequences:
    allalpha.extend(list(np.unique(np.asarray(seq))))
#instantiating vmm and training it
beatlizer = vmm.VMM(alphabet = allalpha )
beatlizer.learn(sequences, 3)

"""
Generating sequence and converting it back to a playable format
"""
from style_machine.lib_table import piano_roll_to_table
from style_machine.lib_midi_write import write_table_to_midi

#generate sequence
song_p_roll = beatlizer.generate_sequence(3000,3)

#converting it back to a pianoroll
on_set = converter.convert_ints_to_vps(song_p_roll).T.astype(bool)

#write Midi
grid = make_grid()
table = piano_roll_to_table(on_set, grid, min_pitch=36)
write_table_to_midi(table, "first.mid") # change filepath as you see fit