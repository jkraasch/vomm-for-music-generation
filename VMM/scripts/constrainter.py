import numpy as np


class Constraint:
    """
    A representation of Binary Constraints used in constrainedVMM
    """

    def __init__(self, constraint, index):
        self.constraints = constraint #:constraint used on elements in sequence at index
        self.index = np.asarray(index) #:pair of indeces referring to the elements that are constrained by the coonstraint

    def __eq__(self, other):
        """
        Compare Constraints by reference
        :param other: constraint to compare the current constraint to
        :return: True if reference of both constraints are the same
        """
        if isinstance(other, Constraint):
            if self.constraints is other.constraints and self.index is other.index:
                return True
        return False

    def flatten(self):
        """
        Gives out a list with the lambda functions and the index pair
        :return: [lambda function, index pair as np.array]
        """
        flattened = [Constraint(constraint, self.index) for constraint in self.constraints]
        return flattened
