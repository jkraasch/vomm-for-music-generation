import pickle
import vmm
import pandas as pd


def context_to_int(context, symbol_resolution=16):
    x = 0
    for n, c in enumerate(context):
        if int(c)< 0:
            print(c)
        x |= int(c) << (symbol_resolution * n)
    return x


def int_to_context(x, length, symbol_resolution=16):
    return [(x >> (symbol_resolution * n)) & (2 ** symbol_resolution - 1) for n in range(length)]


def get_sequences(sequence, depth):
    #getting all unique subsequences of length depth --> Contexts
    context_list = list(set(zip(*(sequence[i:]
                                  for i in range(depth)))))
    #int representation of contexts
    rep_list = [context_to_int(context)
                for context in context_list]
    dictionary = ({k: v for k, v in zip(context_list, rep_list)})

    return context_list, dictionary


def fillDF(df, sequence, depth):
    #getting all subsequences of lenght depth+1 (context + symbol)
    sub_seqs = map(list, zip(*(sequence[i:] for i in range(depth + 1))))

    #Iterating through all subsequences and counting occurances
    for sub in sub_seqs:
        context = tuple(sub[:depth])
        symbol = sub[depth]
        if depth == 0:
            df[symbol] += 1
        else:
            df.loc[[context], [symbol]] += 1
    return df


def intify(bitlist):

    #Tranforming a bitlist to the int it represents
    out = 0
    for bit in bitlist:
        out = (out << 1) | bit
    return out


def sortViewpoints(dict):
    return sorted(dict.items(), key=lambda x: x[0])


def copy(vomm):
    return vmm.VMM(vomm.masks, vomm.alphabet, vomm.rep_dict, vomm.markovLevels, vomm.prob_mats, vomm.symbol_resolution)


def load(file=None):
    try:
        print('trying')
        masks, alphabet, rep_dict, markovLevels, prob_mats, symbol_resolution = __read(file)
        print('done')

        vomm = vmm.VMM(alphabet, symbol_resolution)
        vomm.masks = masks
        vomm.rep_dict = rep_dict
        vomm.markovLevels = markovLevels
        vomm.prob_mats = prob_mats
        print('all params loaded')
        assert isinstance(vomm, vmm.VMM), "only load VOMM"
        return vomm
    except:
        raise ValueError("File not found, did not update")


def write(vomm, file="./tmp/vmms.p"):
    assert isinstance(vomm, vmm.VMM), "only write VOMM"
    masks = vomm.masks
    alphabet = vomm.alphabet
    rep_dict = vomm.rep_dict
    markovLevels = vomm.markovLevels
    prob_mats = vomm.prob_mats
    symbol_resolution = vomm.symbol_resolution
    with open(file, 'wb') as f:
        pickle.dump(masks, f)
        pickle.dump(alphabet, f)
        pickle.dump(rep_dict, f)
        pickle.dump(symbol_resolution, f)
        markovLevels[0].to_sparse(fill_value=0.0).to_pickle(f)
        prob_mats[0].to_sparse(fill_value=0.0).to_pickle(f)
    print("Vmms are stored")


def __read(file="./tmp/vmms.p"):
    print(file, "file")
    f = open(file, 'rb')
    masks = pickle.load(f)
    alphabet = pickle.load(f)
    rep_dict = pickle.load(f)
    symbol_resolution = pickle.load(f)
    print(symbol_resolution)
    markovLevels = [pd.read_pickle(f)]
    prob_mats = [pd.read_pickle(f)]
    f.close()
    return masks, alphabet, rep_dict, markovLevels, prob_mats, symbol_resolution
