import numpy as np


class Convert:
    """
    Class usable to convert np.ndarray(int) to a 1D np.array(int)
    """
    vps_int = np.empty(0)#:np.array that stores the values needed for the conversion of vp to int
    int_vps = np.empty(0)#:np.array that stores the values needed for the conversion of int to vp
    modulo = np.empty(0)#:np.array that stores the values used for the modulo function needed for the conversion of vp to int

    def __init__(self, n_vp, alphabet_sizes):
        """
        Creates arrays for converting viewpoints to ints
        :param n_vp: Number of Viewpoints
        :param alphabet_sizes: Alphabet Sizes
        """
        self.vps_int = np.empty(0)#:np.array that stores the values needed for the conversion of vp to int
        self.int_vps = np.empty(0)#:np.array that stores the values needed for the conversion of int to vp
        self.modulo = np.empty(0)#:np.array that stores the values used for the modulo function needed for the conversion of vp to int
        alphab = list(reversed(alphabet_sizes))
        converter = np.ones(n_vp)
        counter = n_vp
        for j in range(n_vp):
            value = 1
            for i in range(counter - 1):
                value *= alphab[i]
            counter -= 1
            converter[j] *= value
        self.vps_int = converter
        self.int_vps = np.ones(n_vp)
        self.modulo = np.ones(n_vp).astype(int)
        sorted_alphab = alphabet_sizes
        for i in range(len(sorted_alphab)):
            value = sorted_alphab[i]
            self.modulo[i] = value

    def convert_vps_to_ints(self, vps):
        """
        Converts viewpoints to their int representation
        :type vps np.ndarray(int)
        :param vps: Viewpoints to convert to their int representation
        :return: int representation
        """
        vps_t = np.transpose(vps)
        converted = (lambda x: (x * self.vps_int).sum(axis=1))(vps_t)
        converted = converted.astype(object)
        if (converted < 0).any():
            raise ValueError("Overflow")
        return converted

    def convert_ints_to_vps(self, ints):
        """
        Converts int representations to their respective viewpoints
        :type ints: np.array(int)
        :param ints: Int representation to convert to viewpoints
        :return: 2D array of viewpoints
        """
        converted = np.ones((ints.size, self.vps_int.size))
        converted /= self.vps_int
        converted = (converted.T * ints).T.astype(np.int64)
        converted %= self.modulo
        return converted.T
