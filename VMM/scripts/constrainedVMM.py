import copy
import pandas as pd
import csp
import vmm
import numpy as np
import helper


class VMMCSP(csp.CSP):

    """
    An extension to a normal CSP-solver that allows a VMM to be adapted to generate sequences to its learned distribution
    while being a solution of the CSP

    """
    transition_list = {}#:dictionary that contains the vmm that has to be used considering the states domain {tuple(domain): vmm}

    def __init__(self, alphabet, vomm):
        assert isinstance(vomm, vmm.VMM)
        self.vmm = vomm
        csp.CSP.__init__(self, alphabet)

    def solve(self):
        """
        Solves CSP creates the VMMs needed to generate wanted sequences
        """
        self.apply_unary()
        self.check_markovian()
        for domain in self.domains:
            x = helper.copy(self.vmm)
            self.transition_list[tuple(domain)] = x
        self.updateVMM()

    def check_markovian(self):
        """
        Reduce domains so that only markovian transition possible
        if transition probability is 0 reduce domain
        """
        to_check = []
        for i in range(len(self.domains)):
            if len(self.domains[i]) == 1:
                to_check.append(i)
        for id in to_check:  # type: int
            domain = []
            if id > 0:
                for char in self.domains[id - 1]:
                    if self.vmm.probabilities(self.domains[id], np.asarray([char])) != 0:
                        domain.append(char)
                self.domains[id - 1] = domain
                domain = []
            if id < len(self.domains) - 1:

                for char in self.domains[id + 1]:
                    if self.vmm.probabilities(char, np.asarray(self.domains[id])) != 0:
                        domain.append(char)
                self.domains[id + 1] = domain
        if not (all(self.domains)):
            raise Exception("Could not be solved")

    def updateVMM(self):
        """
        Reduce possible symbols to generate according to domain
        --> remove symbols that are not in domain (seeds stay unchanged/ only remove columns)
        """
        for domain in self.domains:
            new_vmms = []
            for old in self.transition_list[tuple(domain)].prob_mats:
                new_vmms.append(old[domain].copy())
            self.transition_list[tuple(domain)].prob_mats = new_vmms
        for depth in range(len(self.transition_list[tuple(self.domains[0])].prob_mats)):
            self.renormalize(depth)

    def renormalize(self, depth):
        """
        Renormalizes VMMs so that they fit the markov distribution of the original vmm
        """
        alpha = 1
        for id in range(len(self.domains) - 1, -1, -1):
            vomm = self.transition_list[tuple(self.domains[id])]
            vomm.alphabet = self.domains[id]
            if type(alpha) == pd.DataFrame:
                alpha = alpha[list(set(vomm.prob_mats[depth].columns) & set(alpha.columns))].copy()
                vomm.prob_mats[depth] = vomm.prob_mats[depth].mul(alpha, axis=1, fill_value=1)

            if depth == 0:
                new_alpha = vomm.prob_mats[depth].sum()
            else:
                new_alpha = vomm.prob_mats[depth].sum(axis=1)
            if type(new_alpha) == pd.Series:
                vomm.prob_mats[depth] = vomm.prob_mats[depth].div(new_alpha.values, axis=0)
            else:
                vomm.prob_mats[depth] /= new_alpha
            vomm.prob_mats[depth] = vomm.prob_mats[depth].fillna(0)
            alpha = new_alpha

    def generate(self, max_depth):
        """
        generates sequence according to constraints
        :param max_depth: maximal order of vmm for the generation
        :return: sequence solving csp
        """
        if max_depth > len(self.transition_list[tuple(self.domains[0])].prob_mats):
            raise ValueError('not enough layers in vmm')
        if max_depth < 0:
            raise ValueError('max_depth is negative')
        sequence = []
        if self.binary.empty:
            for domain in self.domains:
                model = self.transition_list[tuple(domain)]
                sequence.append(model.sample(max_depth, np.asarray(sequence)))
            return np.asarray(sequence)

        # initiating starting parameters for binary backtrracking sampling
        constraintstack = self._getconstraintstack()  # all constraints in one list (flattened)
        id = 0
        startstate = [copy.deepcopy(self.domains), id, copy.deepcopy(constraintstack)]
        memory_stack = [startstate]
        while id < len(self.domains):
            domain = self.domains[id]
            model = self.transition_list[tuple(domain)]
            sequence.append(model.sample(max_depth, np.asarray(sequence)))
            if id in self.binary.columns:
                for constraint in constraintstack:
                    if id == constraint.index[0]:
                        t_id = constraint.index[1]
                        # TRYING to SOLVE constraint
                        try:
                            #print('TRYING', self.domains[id], self.domains[t_id])
                            """
                            Worked out
                                --> reduced domain of target id
                                --> remove constraint from constrainstack
                                --> append newstate to memory_stack
                            """
                            self.domains[t_id] = self.apply_binary((sequence[id], self.domains[t_id]),
                                                                   constraint.constraints)
                            copy_constraintstack = copy.deepcopy(constraintstack)
                            for i in range(len(copy_constraintstack)):
                                if copy_constraintstack[i] == constraint:
                                    copy_constraintstack.pop(i)
                            nextstate = [copy.deepcopy(self.domains), id, copy.deepcopy(copy_constraintstack)]
                            memory_stack.append(nextstate)
                            #print('WORKED', self.domains[id], self.domains[t_id])
                            self.transition_list[tuple(self.domains[t_id])] = helper.copy(self.vmm)
                            self.transition_list[tuple(self.domains[id])] = helper.copy(self.vmm)
                        except:
                            """
                            Did not work out
                                -->check if domain[id] not empty
                                    /// if empty pop memorystack
                                -->if memorystack empty == not solvable
                                -->pop memorystack
                                -->remove sample from domain[id]
                                -->append adapted state
                            """
                            #print('FAILED', self.domains[id], self.domains[t_id])
                            if not self.domains[id]:
                                #print ('EMPTY DOMAIN')
                                memory_stack.pop()
                            if len(memory_stack) == 0:
                                raise Exception('Not Solvable')
                            self.domains, id, constraintstack = memory_stack.pop()
                            self.domains[id].remove(sequence[id])
                            updatedstate = [copy.deepcopy(self.domains), id, copy.deepcopy(constraintstack)]
                            if self.domains[id]:
                                memory_stack.append(updatedstate)
                            else:
                                if len(memory_stack) == 0:
                                    raise Exception('Not Solvable')
                                self.domains, id, constraintstack = memory_stack.pop()
                            sequence = sequence[:id]

                            self.transition_list[tuple(self.domains[t_id])] = helper.copy(self.vmm)
                            self.transition_list[tuple(self.domains[id])] = helper.copy(self.vmm)
                            id -= 1

            id += 1
            self.updateVMM()
        return sequence

    def crossentropy(self, sequence):
        """
        Calculates the cross-entropy for a sequence given the original vmm
        :param sequence: sequence to test (np.array)
        :return: cross-entropy
        """
        return self.vmm.cross_entropy(sequence)

    def _getconstraintstack(self):

        constraint_tuples = []
        # parse through constrains
        for index in self.binary.index:
            for column in self.binary.columns:
                constraint = self.binary.loc[index][column]
                # if not nan extend list by constraint
                if type(constraint) != float:
                    # flatten if constraint is a list

                    if isinstance(constraint.constraints, list):
                        constraint = constraint.flatten()
                    else:
                        constraint = [constraint]
                    constraint_tuples.extend(constraint)
        return constraint_tuples
