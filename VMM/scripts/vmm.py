"""
Jonas Kraasch
June 21 2018

An implementation of VMM with a matrix representation
"""
import pandas as pd
import numpy as np
import itertools

from numpy.core.multiarray import ndarray

import helper
import operator
import pickle
from functools import reduce


class VMM:
    """
    An implementation of a VMM for number sequences usable for music generation

    """
    masks = np.empty(0)#:The bit arrays used to filter contexts with a certain hamming distance
    alphabet = []#:list of chars found in sequence
    rep_dict = {}#:Mapping from the context tuples to the int representation
    markovLevels = []#:The Counts at each order stored in a list of pandas
    prob_mats = []#:The transition Matrices for every order in the VMM
    symbol_resolution = 16#:The contexts will be represented as ints, as such we can specify how many bits should be used for the representation

    def __init__(self, masks=np.empty(0), alphabet=[], rep_dict={}, markovLevels=[], prob_mats=[],
                 symbol_resolution=16):
        """
        Initialize VOMM
        :type alphabet: np.array(int)
        :type symbol_resolution: int
        :param alphabet: alphabet to use for learning
        :param symbol_resolution: resolution of binarization of contexts
        """
        self.masks = masks
        self.alphabet = alphabet
        self.rep_dict = rep_dict
        self.markovLevels = markovLevels
        self.prob_mats = prob_mats
        self.symbol_resolution = symbol_resolution

    def learn(self, sequences, depth):
        """
        Traines VMM for certain orders on a list of np.arrays(int)
        :param sequences: list of np.arrays(int) = Data Set
        :param depth: Highest order of VOMM
        """
        if not self.alphabet: raise ValueError("No alphabet set")
        if depth < 0: raise ValueError("negative depth")
        i = 0
        #creating our VMM by appending the future count Matrices (Dataframes) to our list
        while (len(self.markovLevels) <= depth):
            # Pandas DataFrames have a problem with None or an EmptyList as a row Index so we use a Series for 0-order
            if i == 0:
                self.markovLevels.append(pd.Series())
            else:
                self.markovLevels.append(pd.DataFrame())
            i += 1
        i = 0

        #Iterating over the sequences in the Dataset and counting the occurances
        for sequence in sequences:
            #We only want ot go up to the order that can fit our sequence
            if depth > sequence.shape[0]:
                order = sequence.shape[0]
            else:
                order = depth
            self.__updatePandas(sequence, order)
            i += 1
        self.__createProbMat()

    def predict(self, symbol, pre_cont=np.empty(0)):
        """
        Returns probability of the symbol appearing after a certain context (using the escape methods
        :type symbol: int
        :type pre_cont: np.array(int)
        :param symbol: symbol to predict
        :param pre_cont: prior context
        :return: double probability
        """
        if (symbol not in self.alphabet): raise ValueError("Symbol not in alphabet")
        if (pre_cont.size > len(self.markovLevels)): pre_cont = pre_cont[:-len(self.prob_mats)]
        if pre_cont.size == 0: return self.prob_mats[0][symbol]
        #converting np.array to tuple so it is a usable index of a Pandas Dataframe/Series
        context = tuple(pre_cont.tolist())
        #Escaping if context has not appeared while learning
        if (not (context in self.prob_mats[len(context) - 1].index)
                or self.prob_mats[len(context)].loc[[context], symbol].values[0] == 0):
            return self.predict(symbol, pre_cont[1:])
        return self.prob_mats[len(context)].loc[[context], symbol].values[0]

    def probabilities(self, symbol, pre_cont=np.empty(0)):
        if pre_cont.size == 0: return self.prob_mats[0][symbol]
        context = tuple(pre_cont.tolist())
        if (not (context in self.prob_mats[len(context)].index)):
            return 0.0
        return self.prob_mats[len(context)].loc[[context], symbol].values[0]

    def cross_entropy(self, testSequence, context=np.empty(0)):
        """
        Calculates the cross entropy given a testSequence and a context
        :type context: np.array(int)
        :type testSequence: np.array(int)
        :param testSequence: to test
        :param context: context the test should take place in
        :return: cross entropy
        """
        cross_ent = 0
        for i, symbol in enumerate(testSequence):
            new_context = np.append(context, testSequence[:i])
            cross_ent += np.log(self.predict(symbol, new_context))
        return -cross_ent / len(testSequence)

    def predict_with_mask(self, symbol, pre_cont, distance=1):
        """
        Returns probability of the symbol appearing after similar contexts to the input context (using hamming-distance as similarity measure)
        :type symbol: int
        :type pre_cont: np.array(int)
        :type distance: int
        :param symbol: symbol to predict
        :param pre_cont: context to check prob of symbol
        :param distance: Hamming-distance used for finding similar contexts
        :return: double probability
        """
        if (symbol not in self.alphabet): raise ValueError("Symbol not in alphabet")
        if pre_cont.size == 0: return self.prob_mats[0][symbol]
        context = tuple(pre_cont.tolist())
        #Escaping if context has not appeared while learning
        if (not (context in self.prob_mats[len(context) - 1].index)):
            return self.predict_with_mask(symbol, pre_cont[1:])

        #Getting all known Contexts and filter through them with masks representing the wanted hamming distance
        c = self.rep_dict[context]
        index = np.array([self.rep_dict[key] for key in self.prob_mats[len(context)].index.values])
        if not (self.masks.shape[0] == context.size):
            self.masks = self.__create_masks(context.size, distance)
        idx = np.any([(c & mask) & index == ((c & mask)) for mask in self.masks], 0)

        #Escaping if the mean distribution of all the similar contexts don't add up to 1
        if self.prob_mats[len(context)].iloc[idx].mean().values[0] == 0:
            return self.predict_with_mask(symbol, pre_cont[1:])

        return self.prob_mats[len(context)].iloc[idx].mean().values[0]

    def sample(self, max_depth, pre_cont=np.empty(0)):
        """
        Samples the next symbol after a seed
        :type max_depth: int
        :type pre_cont: np.array(int)
        :param max_depth: maximal order that should be used for sampling
        :param pre_cont: The context the sample should follow
        :return: sampled symbol
        """
        if max_depth < 0: raise ValueError("max_depth is negative")
        if max_depth > len(self.prob_mats): raise ValueError("not enough layers in vmm")

        #For 0-Order have to use pd.Series Indexing
        if pre_cont.size == 0:
            return np.random.choice(self.alphabet, p=self.prob_mats[0].values)

        #Cutting the input context so it abides to max_depth
        if pre_cont.size > max_depth: pre_cont = pre_cont[-max_depth:]
        context = tuple(pre_cont.tolist())


        #Escaping if context has not appeared while learning
        if (not (context in self.prob_mats[len(context)].index)
                or self.prob_mats[len(context)].loc[[context]].values[0].sum() == 0):
            return self.sample(max_depth, pre_cont[1:])

        return np.random.choice(self.alphabet, p=self.prob_mats[len(context)].loc[[context]].values[0])

    def generate_sequence(self, length, max_depth, primer=np.empty(0)):
        """
        Calls sample method "length" many times to generate a sequence
        :type length: int
        :type max_depth: int
        :type primer: np.array(int)
        :param length: length of sequence
        :param max_depth: maximal order that should be used for sampling
        :param primer: The context the sequence should follow
        :return np.array(int) sequence
        """
        if max_depth < 0: raise ValueError("max_depth is negative")
        if length < 0: raise ValueError("length is negative")
        if max_depth > len(self.prob_mats): raise ValueError("not enough layers in vmm")
        sequence = primer
        for i in range(length):
            sampled = self.sample(max_depth, sequence)
            sequence = np.append(sequence,sampled)
        return np.asarray(sequence)

    def sample_with_masks(self, max_depth, context, distance=1):
        """
        Samples the next sequence considering seeds that have a certain Hamming-Distance to the context-argument
        :type max_depth: int
        :type context: np.array(int)
        :type distance: int
        :param max_depth: Maximal order that should be used for sampling
        :param context: Original seed whose similar context are used for sampling
        :param distance: Hamming-distance used for finding similar contexts
        :return sampled symbol
        """

        # Handling Special Cases
        if max_depth < 0: raise ValueError("max_depth is negative")
        if max_depth > len(self.prob_mats): raise ValueError("not enough layers in vmm")
        if context.size == 0: return np.random.choice(self.alphabet, p=self.prob_mats[0].values)
        if context.size > max_depth:
            context = context[-max_depth:]
        if not (tuple(context.tolist()) in self.rep_dict.keys()):
            return self.sample_with_masks(max_depth, context[1:])

        # Getting the mean distribution from all similar contexts
        c = self.rep_dict[tuple(context.tolist())]
        index = np.array([self.rep_dict[key] for key in self.prob_mats[context.size].index.values])

        #If the masks are not in the correct shape, create new, filtering afterwards
        if not (self.masks.shape[0] == context.size):
            self.masks = self.__create_masks(context.size, distance)
        idx = np.any([(c & mask) & index == ((c & mask)) for mask in self.masks], 0)

        #Escaping
        if self.prob_mats[len(context)].iloc[idx].sum(axis=1).mean() != 1:
            sampled = self.sample_with_masks(max_depth, context[1:])
            return sampled

        return np.random.choice(self.alphabet, p=self.prob_mats[context.size].iloc[idx].mean().values)

    def generate_sequence_with_masks(self, length, max_depth, primer, distance=1):
        """
        Calls sample_with mask method "length" many times to generate a sequence using the mean distribution of similar seeds to the input seed
        :type length: int
        :type max_depth: int
        :type primer: np.array(int)
        :type distance: int
        :param length: length of sequence
        :param max_depth: maximal order that should be used for sampling
        :param primer: Original seed whose similar context are used for sequencing
        :param distance: Hamming-distance used for finding similar contexts
        :return np.array(ints) sequence
        """
        if max_depth < 0: raise ValueError("max_depth is negative")
        if length < 0: raise ValueError("length is negative")
        if max_depth > len(self.prob_mats): raise ValueError("not enough layers in vmm")
        sequence = primer
        for i in range(length):
            sampled = self.sample_with_masks(max_depth, sequence, distance)
            sequence = np.append(sequence, sampled)
        return np.asarray(sequence)

    def __updatePandas(self, sequence, depth):
        """
        Increments the counts found in markovLevels
        :param sequence: np.array to get the counts from
        :param depth: int maximal order to learn to
        """
        for level in range(depth + 1):
            index, dictionary = helper.get_sequences(sequence, level)
            self.rep_dict.update(dictionary)
            if level == 0:
                df = pd.Series(data=0, index=self.alphabet)
            else:
                df = pd.DataFrame(data=0, index=index, columns=self.alphabet)
            filled = helper.fillDF(df, sequence, level)
            self.markovLevels[level] = self.markovLevels[level].add(filled, fill_value=0)

    def __createProbMat(self):
        """
        Computes the probabilities
        """
        for pandas in self.markovLevels:
            self.prob_mats.append((pandas.T / pandas.T.sum()).T.fillna(0))

    def __create_masks(self, length, distance=1):
        """
        Creates the masks used for finding contexts with a certain Hamming-Distance
        We create arrays that represent what the bits of our contexts should look like
        :param length: int length of the context that we use
        :param distance: int Hamming-Distance to create the masks for
        :return: np.array of masks
        """
        if distance > length: raise ValueError('Can not reach this distance with context of length', length)
        if length < 0: raise ValueError("length is negative")

        #creating lists that filter the contexts so they abide to the max distance
        #one list per every permutation wher "distance"- many values are set to [[0,0,0]] --> Representing that that
        #byte is not of interest
        masks = np.array([None] * length)
        masks[:distance] = [[0, 0, 0]]
        masks[distance:] = [[1, 1, 1]]
        return (np.asarray(
            list(set([helper.intify(reduce(operator.concat, elem)) for elem in itertools.permutations(masks)]))))

    def load(self, file="./tmp/vmms.p"):
        """
        Loads a VMM from a file and overwrites current VMM with the one loaded
        :param file: Space VMM is stored at
        """
        try:
            self.__read(file)
            print('all params loaded')
        except:
            raise ValueError("File not found, did not update")

    def write(self, file="./tmp/vmms.p"):
        """
        Store VMM at path
        :param file: Path of file to write
        """
        depth = len(self.markovLevels)
        with open(file, 'wb') as f:
            pickle.dump(depth, f)
            pickle.dump(self.masks, f)
            pickle.dump(self.alphabet, f)
            pickle.dump(self.rep_dict, f)
            pickle.dump(self.symbol_resolution, f)
            for i in range(depth):
                self.markovLevels[i].to_sparse(fill_value=0.0).to_pickle(f)
            for i in range(depth):
                self.prob_mats[i].to_sparse(fill_value=0.0).to_pickle(f)
        print("Vmms are stored")

    def __read(self, file):
        print(file, "file")
        print('+++++++++++++')
        f = open(file, 'rb')
        print('------')
        depth = pickle.load(f)
        self.masks = pickle.load(f)
        self.alphabet = pickle.load(f)
        self.rep_dict = pickle.load(f)
        self.symbol_resolution = pickle.load(f)
        markovLevels = []
        for i in range(depth):
            unpickled = pd.read_pickle(f)
            markovLevels.append(unpickled)

        self.markovLevels = markovLevels
        prob_mats = []
        for i in range(depth):
            unpickled = pd.read_pickle(f)
            prob_mats.append(unpickled)

        self.prob_mats = prob_mats
        f.close()
        print('_____')
