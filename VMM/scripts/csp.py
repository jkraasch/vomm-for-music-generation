import constrainter
import numpy as np
import pandas as pd


class CSP:
    unary = {} #:Dictionary {index: Constraint}
    binary = pd.DataFrame()#:pd.Dataframe (column, index) = variables that should be constrained, data = constraint
    domains = []#:list of domains (list(int)) at every state
    alphabet = []#:list(int) possible values in CSP

    def __init__(self, alphabet):
        """
        Initializing CSP
        :type alphabet: list(int)
        :param alphabet: Alphabet used in VMM to constrain
        """
        self.alphabet = np.asarray(alphabet)

    def create_constraints(self, R1, R2=[]):
        """
        Creates constraints out of input to later use in sequence generation
        :type R1: np.array(unary lambda function)
        :type R2: list(list(tuple(int), binary lambda function))
        :param R1: Unary constraints stored as such: [None, setvalue,...]
        :param R2: Binary constraints stored as such: [[(id1,id2), [binary constraint]] [(x,y),[next constraint]]]
        """
        idx = np.where(R1)
        constraints = np.asarray(R1[idx])
        self.unary = dict(zip(idx[0], constraints))
        for _ in range(len(R1)):
            self.domains.append(list(self.alphabet))
        if R2.any():
            for pair, const in R2:
                x, y = pair
                if x > y:
                    buffer = x
                    x = y
                    y = buffer
                constlist = constrainter.Constraint(const, [x, y])
                self.binary = self.binary.append(pd.DataFrame(data=constlist, index=[y], columns=[x]))

    def apply_unary(self):
        """
        Applies unary constraints to the domains of the variables, reduces their domains
        """
        for id, constraint in self.unary.iteritems():
            self.domains[id] = list(self.alphabet[np.where(np.vectorize(constraint)(self.alphabet))])

        if not (all(self.domains)):
            raise Exception("Could not be solved")

    def apply_binary(self, domains, constraint):
        """
        Applies binary constraint to second domain in domains-argument considering a binary lambda function and the first domain in the domains-argument
        :type domains: tuple(int, list(int))
        :type constraint: binary lambda function
        :param domains: Domains of the states to constrain
        :param constraint: Binary constraint to apply
        :return: Reduced domain for targeted state
        """
        set = domains[0]
        new_domain = np.asarray(domains[1])
        id = np.vectorize(constraint)([set], new_domain)
        new_domain = list(new_domain[np.where(np.asarray(id))])
        if new_domain:
            return new_domain
        raise Exception('No solution, backtrack')
